package com.SpringBootdemo.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.util.StreamUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import com.SpringBootdemo.airport.AirportLocation;
import com.SpringBootdemo.exception.MyResourceNotFoundException;
import com.SpringBootdemo.oauth.AccessConfig;

@Service
public class AirportService {

	@Autowired
	@Qualifier("oAuth2Rest")
	private RestTemplate restTemplate;
	
	/*@Value("${airportsurl}")
	private String airportsAPIUrl;
	@Value("${airportcode}")
	private String airportcodeAPIUrl;*/
	@Autowired
	AccessConfig accessConfig;
	
	
	public ResponseEntity<AirportLocation> retrieveAirportCode() 
	{
		try {
			
		 ResponseEntity<AirportLocation> result = restTemplate.exchange(accessConfig.getAirportsUrl(),HttpMethod.GET,null, AirportLocation.class);
		//  AirportLocation result = restTemplate.getForObject(accessConfig.getAirportsUrl(), AirportLocation.class);
		 
		 return result;
		
		}
		 catch (MyResourceNotFoundException exc) {
	         throw new ResponseStatusException( HttpStatus.NOT_FOUND, "Airport Not Found", exc);
	    }
		
	}
	
	
}
