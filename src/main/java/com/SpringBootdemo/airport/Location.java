package com.SpringBootdemo.airport;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;



import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)

public class Location {

    public Location(String code, String name, String description) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
	}

	private String code, name, description;
   

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	  @Override
	    public String toString() {
	        return "Code =" + code + ", firstName=" + name + ", lastName=" + description;
	    }

		public Location getInputStream() {
			// TODO Auto-generated method stub
			return null;
		}

		public Object contains(String string) {
			// TODO Auto-generated method stub
			return null;
		}
		
	
}
