package com.SpringBootdemo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.SpringBootdemo.airport.AirportInterface;
import com.SpringBootdemo.airport.Location;
import com.SpringBootdemo.exception.MyResourceNotFoundException;
import com.SpringBootdemo.model.AirportModel;

import com.couchbase.client.core.deps.com.fasterxml.jackson.annotation.JsonPropertyOrder;
@ControllerAdvice

@RestController
//@RequestMapping("/airports")
@JsonPropertyOrder("jsonData")
public class AirportController {

	@Autowired
	private AirportModel airportModel;
   // @RequestMapping(value ="/airports", method = RequestMethod.POST)
 //@RequestMapping(method = GET)
//@RequestMapping("/airports")
 //@PostMapping("/airports")
@RequestMapping(value = "/airports", method = RequestMethod.GET)
 public  List<Location> list( ) throws Exception  {
	
	try {
	return airportModel.list();
	
		}
	 catch (MyResourceNotFoundException exc) {
         throw new ResponseStatusException( HttpStatus.NOT_FOUND, "Airport Not Found", exc);
    }
}

}
