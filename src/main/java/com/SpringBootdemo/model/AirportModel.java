package com.SpringBootdemo.model;

import com.SpringBootdemo.airport.AirportInterface;
import com.SpringBootdemo.airport.AirportLocation;

import com.SpringBootdemo.airport.Location;
import com.SpringBootdemo.exception.MyResourceNotFoundException;
import com.SpringBootdemo.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;




@Slf4j
@Repository
public class AirportModel implements AirportInterface {
	
	
	@Autowired
	private AirportService airportService;
	public List<Location> list()  
	{
		try {
		
		 ResponseEntity<AirportLocation> result = airportService.retrieveAirportCode();
		
		List<Location> airport = result.getBody().getLocationList();
		List<Location> airport1 = new ArrayList<>();
		for (Location loc : airport) {
			loc.setCode(airport.get(0).getCode());
			loc.setName(airport.get(1).getName());
			loc.setDescription(airport.get(2).getDescription());
			airport1.add(loc);
		}
		
		//List<Location> location2=employees.ge();
		return airport1;
		}
		 catch (MyResourceNotFoundException exc) {
	         throw new ResponseStatusException( HttpStatus.NOT_FOUND, "Airport Not Found", exc);
	    }
		
	}

	
	

}
