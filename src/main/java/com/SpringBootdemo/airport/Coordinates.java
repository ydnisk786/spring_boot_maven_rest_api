package com.SpringBootdemo.airport;

import lombok.Value;

@Value
public class Coordinates {

    private double latitude, longitude;

}
