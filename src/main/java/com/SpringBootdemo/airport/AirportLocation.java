package com.SpringBootdemo.airport;

import java.util.ArrayList;
import java.util.List;

public class AirportLocation {
	
	private List<Location> locationList;
	
	public AirportLocation(List<Location> locationList) {
		super();
		this.locationList = locationList;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	
	
	public AirportLocation() {
		locationList = new ArrayList<>();
    }

}
